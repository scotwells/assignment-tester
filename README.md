ListString Tester
=================

I've put together a small bash script that helps with testing the ListString assignment. This script does not help with the completion of the project, it only helps testing to make sure all the tests that were given and any tests you make will pass. I have only tested this script on **OS X 10.9**, **Linux Mint**, and **UCF's eustis server**.

If you have `valgrind` installed on your computer the script will ask you if you would like to enable memcheck. Enabling valgrind will result in slower test results especially in longer running test cases.  

### How to use the script

1. Download the zip files and unpack the contents.
2. Copy `runTest` into your projects root directory.
3. `cd` into your projects root directory ex. `cd /path/to/project/folder/`
4. Run the script with `./runTests`
5. Choose the `ListString.c` file you created.
6. Specify the location of the input and output files.
7. You should now be ready to run tests.

####Finding Tests
The script expects the input and output files to be in the specified format `input{{number}}.txt` and `output{{number}}.txt`. When you start the script it will parse the specified input and output folders to find your tests. If the input files do not match this format then theres no guarantee the script will be able to find your tests. You can also add or remove tests once in the script.

####Running Tests
Each time you run a test, your `ListString.c` file is recompliled and run with the specified input file. After the test is run and if the program executes successfully, a diff is run on the output generated and the matching output file that was found for the input file. 

#####Runtime

I couldn't find a simple cross compatible way to get the exact runtime of the program so instead I have it giving you an approximate runtime to the nearest second. 


#####Early Termination
The script runs the tests in the background and monitors its runtime. Once the runtime has exceeded *10 seconds* you will be prompted to terminate the test. I added this feature to protect againt getting stuck in infinite loops and having to exit the script entirely. 

#####Logging

Instead of outputing all the test results to the screen, the script places everything into log files saved in the `logs` directory that is created in the project directory. When running tests individually, you'll be given the option to view these log files in the terminal using `less` once the test is complete. If you haven't used `less` before, you can exit the screen by pressing `q`.

If you find any bugs you can contact me at [wells.scot@knights.ucf.edu](mailto:wells.scot@knights.ucf.edu) or through webcourses.